import java.io.*
import java.util.*
import java.util.zip.Deflater
import java.util.zip.DeflaterOutputStream
import java.util.zip.InflaterInputStream

interface DataSource {
    fun writeData(data: String)

    fun readData(): String
}

class FileDataSource(private val name: String) : DataSource {

    override fun writeData(data: String) {
        val file = File(name)
        try {
            FileOutputStream(file).use { fos -> fos.write(data.toByteArray(), 0, data.length) }
        } catch (ex: IOException) {
            println(ex.message)
        }

    }

    override fun readData(): String {
        var buffer: CharArray? = null
        val file = File(name)
        try {
            FileReader(file).use { reader ->
                buffer = CharArray(file.length().toInt())
                reader.read(buffer!!)
            }
        } catch (ex: IOException) {
            println(ex.message)
        }

        return String(buffer!!)
    }
}

open class DataSourceDecorator internal constructor(private val wrappee: DataSource) : DataSource {

    override fun writeData(data: String) {
        wrappee.writeData(data)
    }

    override fun readData(): String {
        return wrappee.readData()
    }
}

class EncryptionDecorator(source: DataSource) : DataSourceDecorator(source) {

    override fun writeData(data: String) {
        super.writeData(encode(data))
    }

    override fun readData(): String {
        return decode(super.readData())
    }

    private fun encode(data: String): String {
        val result = data.toByteArray()
        for (i in result.indices) {
            result[i] = (1.toByte() + result[i]).toByte()
        }
        return Base64.getEncoder().encodeToString(result)
    }

    private fun decode(data: String): String {
        val result = Base64.getDecoder().decode(data)
        for (i in result.indices) {
            result[i] = (1.toByte() - result[i]).toByte()
        }
        return String(result)
    }
}

class CompressionDecorator(source: DataSource) : DataSourceDecorator(source) {
    var compressionLevel = 6

    override fun writeData(data: String) {
        super.writeData(compress(data)!!)
    }

    override fun readData(): String {
        return decompress(super.readData()).toString()
    }

    private fun compress(stringData: String): String? {
        val data = stringData.toByteArray()
        try {
            val bout = ByteArrayOutputStream(512)
            val dos = DeflaterOutputStream(bout, Deflater(compressionLevel))
            dos.write(data)
            dos.close()
            bout.close()
            return Base64.getEncoder().encodeToString(bout.toByteArray())
        } catch (ex: IOException) {
            return null
        }

    }

    private fun decompress(stringData: String): String? {
        val data = Base64.getDecoder().decode(stringData)
        try {
            val `in` = ByteArrayInputStream(data)
            val iin = InflaterInputStream(`in`)
            val bout = ByteArrayOutputStream(512)
            var b: Int = iin.read()
            while (b != -1) {
                bout.write(b)
            }
            `in`.close()
            iin.close()
            bout.close()
            return String(bout.toByteArray())
        } catch (ex: IOException) {
            return null
        }

    }
}

object Demo {
    @JvmStatic
    fun main(args: Array<String>) {
        val salaryRecords = "Name,Salary\nJohn Smith,100000\nSteven Jobs,912000"
        val encoded = CompressionDecorator(
            EncryptionDecorator(
                FileDataSource("out/OutputDemo.txt")
            )
        )
        encoded.writeData(salaryRecords)
        val plain = FileDataSource("out/OutputDemo.txt")

        println("- Input ----------------")
        println(salaryRecords)
        println("- Encoded --------------")
        println(plain.readData())
        println("- Decoded --------------")
        println(encoded.readData())
    }
}